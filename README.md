# IDD

Code examples and Jupyter-notebooks for the course Infectious Disease Dynamics, mainly in Julia.


Requirements:
- Jupyter(Lab): https://jupyterlab.readthedocs.io/en/stable/getting_started/installation.html
- Julia (v1.5 or higher): https://julialang.org/downloads/

Coding style:
- as simple as possible
- mark missing or bad code with "TODO: "
- make use of whitespaces to align everything accordingly
- work in separate branch, merge in the end
- frequently used code should be moved to a separate file (TODO)

TODO:
- include summary of first lecture (new file)
- include short summary/theory party of second lecture (ode_models)
- include third lecture with computation guidelines & methods for the stability analysis, R0 & maximum likelihood estimation (new file)
- include fourth lecture with all models, vaccine efficacy (direct, indirect (all types), total) (new file)
- include fifth lecture with intra-host models & mutation rate
